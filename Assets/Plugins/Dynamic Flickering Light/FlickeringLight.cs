﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class FlickeringLight : MonoBehaviour
{


    public delegate void MainLoop();

    public event MainLoop mainLoop;


    [FoldoutGroup("Setup")]
    [OnValueChanged("UpdateConfig")]
    [Tooltip("This is how big the light is. Experiment with it.")]
    public float scale;
    [FoldoutGroup("Setup")]
    [OnValueChanged("UpdateConfig")]
    [Tooltip("The moves (new targets for properties; intensity, range, position) your light will do per second.")]
    public float speed;

    [FoldoutGroup("Setup")]
    public bool MakeSourceStationary;
    [FoldoutGroup("Setup")]
    [HideIf("MakeSourceStationary")]
    public float positionOffset;


    private Light light;

    private float intensityOrigin;
    private float intensityOffset;
    private float intensityDelta;
    private float rangeOrigin;
    private float rangeOffset;
    private float rangeTarget;
    private float rangeDelta;

    private float _positionOffset;
    private float _scale;
    private float _speed;

    private Vector3 positionOrigin;
    private Vector3 positionDelta;

    private bool setNewTargets;
    private bool gradeLightUp;

    private float deltaSum = 0;


    void Start()
    {
        light = GetComponent<Light>();

        intensityOrigin = light.intensity;
        rangeOrigin = light.range;
        positionOrigin = transform.position;

        setNewTargets = true;

        _scale = scale * 0.1f;
        _speed = speed * 0.02f;

        intensityOffset = light.intensity * _scale;
        rangeOffset = light.range * _scale;
        _positionOffset = positionOffset * _scale * 0.1f;

        mainLoop += IntensityAndRange;

        if (!MakeSourceStationary)
        {
            mainLoop += Position;
        }

    }
    private void UpdateConfig()
    {
        if (!Application.IsPlaying(this))
            return;
        light.intensity = intensityOrigin;
        light.range = rangeOrigin;
        deltaSum = 0;
        mainLoop = delegate { };
        transform.position = positionOrigin;
        Start();
    }
    private void IntensityAndRange()
    {

        if (setNewTargets) ////////////////SETTING INTENSITY TARGET IS ENOUGH, AS WE ONLY USE IT TO CHECK IF EVERY PROPERTY HAS REACHED THEIR TARGET. WE CAN DO THIS BECAUSE WE KNOW THEY ALL REACH THEIR TARGETS AT THE SAME TIME.//////
        {
            intensityDelta = (intensityOrigin + Random.Range(-intensityOffset, intensityOffset) - light.intensity) * _speed;


            rangeTarget = rangeOrigin + Random.Range(-rangeOffset, rangeOffset);
            if (rangeTarget > light.range)
                gradeLightUp = true;
            else
                gradeLightUp = false;

            rangeDelta = (rangeTarget - light.range) * _speed;

            setNewTargets = false;
        }

        light.intensity += intensityDelta;
        light.range += rangeDelta;


        if ((gradeLightUp && light.range >= rangeTarget) || (!gradeLightUp && light.range <= rangeTarget)) //////// CHECK IF TARGET IS NEAR ENOUGH. /////////
            setNewTargets = true;




    }
    private void Position()
    {

        if (setNewTargets)
        {
            positionDelta = (positionOrigin + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized * positionOffset - transform.position) * _speed;

        }

        transform.position += positionDelta;


    }
    void Update()
    {

        if (deltaSum >= 0.02f)
        {

            mainLoop();
            deltaSum -= 0.02f;

        }

        deltaSum += Time.deltaTime;
    }
}