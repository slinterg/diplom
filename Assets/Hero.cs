﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Hero : MonoBehaviour
{
    [SerializeField]
    private int speed = 10;
    [SerializeField]
    private Animator animator = null;
    [SerializeField]
    private new SpriteRenderer renderer = null;

    private GameObject target;

    public enum direction
    {
        left,
        right,
        none
    }

    [SerializeField]
    private direction myDirection = direction.none;
    // Start is called before the first frame update
    void Start()
    {
        renderer = gameObject.GetComponent<SpriteRenderer>();
        target = GameObject.FindGameObjectWithTag("Maneken");
    }

    // Update is called once per frame
    void Update()
    {
        if (myDirection == direction.right)
        {
            gameObject.transform.position += new Vector3(speed, 0, 0);
        }
        if (myDirection == direction.left)
        {
            gameObject.transform.position += new Vector3(-speed, 0, 0);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            animator.SetBool("Idle", false);
            animator.SetBool("Run", true);
            renderer.flipX = false;

            myDirection = direction.right;

            Debug.Log("RUN Right!");
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            animator.SetBool("Idle", false);
            animator.SetBool("Run", true);
            renderer.flipX = true;

            myDirection = direction.left;

            Debug.Log("RUN Left!");
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            animator.SetBool("Idle", true);
            animator.SetBool("Run", false);

            myDirection = direction.none;

            Debug.Log("STOP!");
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            animator.SetBool("Idle", true);
            animator.SetBool("Run", false);

            myDirection = direction.none;

            Debug.Log("STOP!");
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            animator.SetBool("Idle", false);
            animator.SetBool("Run", false);
            animator.SetBool("Attack", true);

            target.GetComponent<Animator>().SetBool("Idle", false);
            target.GetComponent<Animator>().SetBool("Damage", true);

            Debug.Log("ATTACK!");
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            animator.SetBool("Idle", true);
            animator.SetBool("Run", false);
            animator.SetBool("Attack", false);

            target.GetComponent<Animator>().SetBool("Idle", true);
            target.GetComponent<Animator>().SetBool("Damage", false);

            Debug.Log("ATTACK STOP!");
        }

        
    }
    //public void OnPointerClick(PointerEventData eventData)
    //{
    //    //обработка ЛКМ
    //    if (eventData.button == PointerEventData.InputButton.Left)
    //    {
    //        animator.SetBool("Idle", false);
    //        animator.SetBool("Run", false);
    //        animator.SetBool("Attack", true);
    //        animator.SetBool("Attack", false);
    //    }
    //    //обработка ПКМ
    //    else if (eventData.button == PointerEventData.InputButton.Right)
    //    {
    //        Debug.Log("RMB Click!");
    //    }
    //    //обработка СКМ
    //    else if (eventData.button == PointerEventData.InputButton.Middle)
    //    {
    //        Debug.Log("МMB Click!");
    //    }
    //}

}
