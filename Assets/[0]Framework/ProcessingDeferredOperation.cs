﻿using System;
using Homebrew;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

namespace HSG
{
    //Процессинг отложенных операций
    //В него можно добавлять инструкции, которые требуют выполнения какого-то условия
    //Например:
    //UnityAction action = null;
    //action = new UnityAction(() => { if (condition)  {some code} });
    //ProcessingDeferredOperation.Default.Add(action);
    //В конце инструкции "some code" нужно удалить инструкцию action из процессинга
    //ProcessingDeferredOperation.Default.Remove(action);

    //ИТОГ: инструкция action будет проигрываться каждый Tick, пока не выполниться и не удалиться
    public class ProcessingDeferredOperation : ITick, IKernel, IDisposable
    {
        public static ProcessingDeferredOperation Default;
        private List<UnityAction> operations;
        private List<bool> autoCatch;
        private List<bool> triggerOnce;

        public void Tick()
        {
            for (int i = 0; i < operations.Count; i++)
            {
                if (operations[i] != null)
                {
                    if (triggerOnce[i])
                    {
                        operations[i].Invoke();
                        Remove(operations[i]);
                        i--;
                        continue;
                    }
                    if (autoCatch[i])
                    {
                        try
                        {
                            operations[i].Invoke();
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                        Remove(operations[i]);
                    }
                    else
                        operations[i].Invoke();
                }
                else
                {
                    Remove(operations[i]);
                    i--;
                }
            }
        }
        public void Add(UnityAction action, bool triggerOnce = false, bool autoCatch = false)
        {
            operations.Add(action);
            this.autoCatch.Add(autoCatch);
            this.triggerOnce.Add(triggerOnce);
        }
        public void Remove(UnityAction action)
        {
            for (int i = 0; i < operations.Count; i++)
                if (operations[i] == action)
                {
                    operations.RemoveAt(i);
                    autoCatch.RemoveAt(i);
                    triggerOnce.RemoveAt(i);
                    i--;
                }
        }

        public void Setup()
        {
            ProcessingDeferredOperation.Default = this;

            ProcessingGroupAttributes.Default.Setup(this);
            ProcessingSignals.Default.Add(this);
            ProcessingUpdate.Default.Add(this);

            if (operations == null)
                operations = new List<UnityAction>();
            if (autoCatch == null)
                autoCatch = new List<bool>();
            if (triggerOnce == null)
                triggerOnce = new List<bool>();

            Debug.Log("[" + DateTime.Now + " Tick: " + DateTime.Now.Ticks + "]: " + "[" + "PROCESSING" + "] " + "[" + this + "]" + "----Setup End");
        }
        public void Dispose()
        {

            Toolbox.Get<ProcessingSignals>().Remove(this);
            ProcessingUpdate.Default.Remove(this);

            Setup();
        }
    }
}
