﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using PixelCrushers.DialogueSystem;

namespace HSG
{
    public class SelectorUpgraded : Selector
    {
        protected override void Update()
        {
            // Exit if disabled or paused:
            if (!enabled || (UnityEngine.Time.timeScale <= 0)) return;

            // Exit if there's no camera:
            if (UnityEngine.Camera.main == null) return;

            // Exit if using mouse selection and is over a UI element:
            //if ((selectAt == SelectAt.MousePosition) && (UnityEngine.EventSystems.EventSystem.current != null) && UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) return;

            // Raycast 2D or 3D:
            switch (runRaycasts)
            {
                case Dimension.In2D:
                    Run2DRaycast();
                    break;
                default:
                case Dimension.In3D:
                    Run3DRaycast();
                    break;
            }

            // If the player presses the use key/button on a target:
            if (IsUseButtonDown()) UseCurrentSelection();
        }
    }
}
