﻿using Homebrew;
using UnityEngine;
using UnityEngine.EventSystems;

public class BehaviorMove : ActorBehavior, ITick
{
    [Bind] private DataMove dataMove;
    [Bind] private DataStates dataStates;
    [Bind] private DataStats dataStats;

    public void Setup(Actor actor)
    {
        Debug.Log(dataMove.x = actor.selfTransform.position.x);
    }
    public void Tick()
    {
        float move = Input.GetAxis("Horizontal");

        Debug.Log(move);

        //определяем, на земле ли персонаж
        bool gr = Physics2D.OverlapCircle(actor.selfTransform.position, 0.2f, dataMove.whatIsGround);
        //устанавливаем в аниматоре значение скорости взлета/падения
        actor.Get<Animator>().SetFloat("vSpeed", actor.Get<Rigidbody2D>().velocity.y);
        //если персонаж в прыжке - выход из метода, чтобы не выполнялись действия, связанные с бегом
        if (dataStates.state == DataStates.State.Jump)
            return;

        //проверяем в воздухе ли персонаж и нажатие кнопки
        if (dataStates.state == DataStates.State.Jump && Input.GetKey(KeyCode.Space))
        {
            dataStates.state = DataStates.State.Jump;

            //прикладываем силу вверх, чтобы персонаж подпрыгнул
            actor.Get<Rigidbody2D>().AddForce(new Vector2(0, 600));
        }


        //если нажали клавишу для перемещения вправо, а персонаж направлен влево
        if (Input.GetKey(KeyCode.D))
        {
            if (!dataStates.isFacingRight)
                dataStates.isFacingRight = true;

            dataStates.state = DataStates.State.Run;

            //отражаем персонажа вправо
            actor.Get<SpriteRenderer>().flipX = false;
            actor.selfTransform.position += new Vector3(dataMove.speed, 0, 0);
        }
        //обратная ситуация. отражаем персонажа влево
        else if (Input.GetKey(KeyCode.A))
        {
            if (dataStates.isFacingRight)
                dataStates.isFacingRight = false;

            dataStates.state = DataStates.State.Run;

            //отражаем персонажа вправо
            actor.Get<SpriteRenderer>().flipX = true;
            actor.selfTransform.position += new Vector3(-dataMove.speed, 0, 0);
        }
        else if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) dataStates.state = DataStates.State.Idle;
               

    }

}