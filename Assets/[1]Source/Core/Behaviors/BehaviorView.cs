﻿using Homebrew;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class BehaviorView : ActorBehavior, ITick
{
    [Bind] private DataStates dataStates;

    private Animator animator;

    protected override void Setup()
    {
        animator = actor.Get<Animator>();

        Debug.Log("[" + DateTime.Now + " Tick: " + DateTime.Now.Ticks + "]: " + "[" + actor + "] " + "[" + this + "]" + "----Setup End");
    }

    public void Tick()
    {
        switch (dataStates.state)
        {
            case DataStates.State.Idle:
                StatesReset();
                animator.SetBool("Idle", true);
                break;

            case DataStates.State.Run:
                StatesReset();
                animator.SetBool("Run", true);
                break;

            case DataStates.State.Jump:
                StatesReset();
                animator.SetBool("Jump", true);
                break;

            //case DataStates.State.Damage:
            //    StatesReset();
            //    animator.SetBool("Damage", true);
            //    break;

            case DataStates.State.Attack:
                StatesReset();
                animator.SetBool("Attack", true);
                break;

            default:
                StatesReset();
                animator.SetBool("Idle", true);
                break;
        }
    }

    private void StatesReset()
    {
        animator.SetBool("Idle",    false);
        animator.SetBool("Jump",    false);
        animator.SetBool("Run",     false);
        //animator.SetBool("Damage",  false);
        animator.SetBool("Attack",  false);
    }
}