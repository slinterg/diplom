﻿using UnityEngine;
using Homebrew;
using UnityEngine.EventSystems;
using System;

namespace HSG
{
    public class ComponentSpriteOutline : MonoCached, IPointerEnterHandler, IPointerExitHandler
    {
        public SpriteRenderer spriteRenderer;
        public Color color = Color.white;

        protected override void HandleEnable()
        {
            if (spriteRenderer == null)
                OnAwake();

            Debug.Log("[" + DateTime.Now + " Tick: " + DateTime.Now.Ticks + "]: " + "[" + "COMPONENT" + "] " + "[" + this + "]" + "----Setup End");
        }
        protected override void OnAwake()
        {
            if (spriteRenderer == null && transform.FindDeep("View") != null)
                spriteRenderer = transform.FindDeep("View").GetComponent<SpriteRenderer>();
            if (spriteRenderer == null)
                spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

            UpdateOutline(false);
        }

        protected override void HandleDisable()
        {
            OnBeforeDestroy();
        }
        protected override void OnBeforeDestroy()
        {
            spriteRenderer = null;
        }
        public void OnPointerEnter(PointerEventData eventData)
        {
            UpdateOutline(true);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            UpdateOutline(false);
        }

        void UpdateOutline(bool outline)
        {
            if (spriteRenderer == null)
                return;
            MaterialPropertyBlock mpb = new MaterialPropertyBlock();
            spriteRenderer.GetPropertyBlock(mpb);
            mpb.SetFloat("_Outline", outline ? 1f : 0);
            mpb.SetColor("_OutlineColor", color);
            spriteRenderer.SetPropertyBlock(mpb);
        }
    }
}