/*===============================================================
Product:    Shoot off their lumps
Developer:  Dimitry Pixeye - pixeye@hbrew.store
Company:    Homebrew - http://hbrew.store
Date:       10/12/2017 14:09
================================================================*/


using UnityEngine;
using Homebrew;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace HSG
{
    [CreateAssetMenu(fileName = "DataGameSession", menuName = "Data/DataGameSession")]
    public class DataGameSession : DataGame
    {
        [HideInInspector]
        public static DataGameSession Default;
        //Settings:
        [FoldoutGroup("DebugSetup")]
        public bool enableProcessingDebug = true;
        [FoldoutGroup("Setup")]
        public float abilityAnimationSpeed;
        [FoldoutGroup("Setup")]
        public float EffectVisualizationTime = 1.5f;
        //SettingsEnd
        [FoldoutGroup("Status")]
        [ReadOnly]
        public List<string> completedScenes;
        [FoldoutGroup("Status")]
        [ReadOnly]
        public List<string> lootedIOs;
        [FoldoutGroup("Status")]
        [ReadOnly]
        public int previousScene;
        [HideInInspector]
        //public DataPlayerSaving dataPlayerSaving;

        public void Awake()
        {
            completedScenes = new List<string>();
            lootedIOs = new List<string>();
        }
        public DataGameSession()
        {
            if (Default == null)
                Default = this;

            abilityAnimationSpeed = 1f;
        }
        //public bool Load(Actor actor)
        //{
        //    if (!dataPlayerSaving.Initialized)
        //        return false;
        //    if (actor is ActorPlayer)
        //    {
        //        ActorPlayer _actor = actor as ActorPlayer;
        //        _actor.dataCharacterStats.lvl3DataDerivativeResultSumStats.curHP = dataPlayerSaving.dataDerivativeStats.curHP;
        //        _actor.dataDerivativeStatsPlayer.curCP = dataPlayerSaving.dataDerivativeStatsPlayer.curCP;
        //        _actor.dataCharacterStats.lvl0DataMainCharacterStats = dataPlayerSaving.dataMainStats;
        //    }
        //    return true;
        //}
        //public void Save(Actor actor)
        //{
            //if (dataPlayerSaving == null)
            //    dataPlayerSaving = new DataPlayerSaving();
            //if (actor is ActorPlayer)
            //{
            //    ActorPlayer _actor = actor as ActorPlayer;
            //    dataPlayerSaving.dataDerivativeStats.curHP = _actor.dataCharacterStats.lvl3DataDerivativeResultSumStats.curHP;
            //    dataPlayerSaving.dataDerivativeStatsPlayer.curCP = _actor.dataDerivativeStatsPlayer.curCP;
            //    dataPlayerSaving.dataMainStats = _actor.dataCharacterStats.lvl0DataMainCharacterStats;
            //}
            //dataPlayerSaving.initialized = true;
        //}
        public void CleanSession()
        {
            SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("SceneGlobal"));
            GameObject.FindObjectOfType<PixelCrushers.DialogueSystem.DialogueSystemController>().ResetDatabase(PixelCrushers.DialogueSystem.DatabaseResetOptions.KeepAllLoaded);
            //dataPlayerSaving.initialized = false;
            //dataPlayerSaving.dataDerivativeStats = new DataDerivativeStats();
            //dataPlayerSaving.dataDerivativeStatsPlayer = new DataDerivativeStatsPlayer();
            //dataPlayerSaving.dataMainStats = new DataMainStats();

            completedScenes.Clear();
            lootedIOs.Clear();
        }
        public override void Dispose()
        {
            completedScenes.Clear();
            lootedIOs.Clear();
        }

    }
}