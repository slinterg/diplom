﻿using Homebrew;
using System.IO;
using UnityEngine;

namespace HSG
{
    [CreateAssetMenu(fileName = "CommandsDefault", menuName = "Commands/CommandsDefault")]
    public class CommandsConsoleDefault : CommandsConsole
    {
        [Bind]
        public void TestCommand(string name)
        {
            Debug.Log(name);
        }
              
    }
}
