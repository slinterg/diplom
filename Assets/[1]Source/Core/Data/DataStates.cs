﻿using Homebrew;

[System.Serializable]
public class DataStates : IData
{
    public enum State
    {
        Idle,
        Run,
        Jump,
        Damage,
        Attack
    }

    public bool isFacingRight;
    public State state;

    public void Dispose()
    {
    }
}