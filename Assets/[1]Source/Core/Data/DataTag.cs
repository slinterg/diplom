﻿using Homebrew;

namespace HSG
{
    [System.Serializable]
    public struct DataTag
    {
        [TagFilter(typeof(Tag))] public int id;
    }
}