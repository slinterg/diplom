﻿using Homebrew;

[System.Serializable]
public class DataStats : IData
{
    public float hp, sp, mp;

    public void Dispose()
    {
    }
}