﻿using Homebrew;
using UnityEngine;

[System.Serializable]
public class DataMove : IData
{
    public float x, y, speed;
    public LayerMask whatIsGround;

    public void Dispose()
    {

    }

    public void Setup(Actor actor)
    {
        Debug.Log(x = actor.selfTransform.position.x);
        
    }
}