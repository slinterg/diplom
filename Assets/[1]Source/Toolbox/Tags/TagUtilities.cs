﻿using Homebrew;

namespace HSG
{
    public static partial class Tag
    {
        [TagField(categoryName = "Utilits/MonoObjects")] public const int Camera = 100;
        [TagField(categoryName = "Utilits/MonoObjects")] public const int BackGround = 101;

        [TagField(categoryName = "Utilits/HP")] public const int EnemyHP = 111;
        [TagField(categoryName = "Utilits/HP")] public const int PlayerHP = 112;

    }
}
