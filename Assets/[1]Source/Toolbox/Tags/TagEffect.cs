﻿using Homebrew;

namespace HSG
{
    public static partial class Tag
    {
        //[TagField(categoryName = "Effect/Physical")] public const int Dead = 2100;

        [TagField(categoryName = "Ability/Type")]
        public const int DealDamage = 2101;
        [TagField(categoryName = "Ability/Type")]
        public const int CastEffect = 2102;
        [TagField(categoryName = "Ability/Type")]
        public const int Magic = 2103;
    }
}
