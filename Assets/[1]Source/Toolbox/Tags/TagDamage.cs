﻿using Homebrew;

namespace HSG
{
    public static partial class Tag
    {
        [TagField(categoryName = "DamageType")] public const int DamageSimple = 1200;
    }
}