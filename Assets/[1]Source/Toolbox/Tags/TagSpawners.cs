﻿using Homebrew;

namespace HSG
{
    public static partial class Tag
    {
        [TagField(categoryName = "Spawners/Entities")] public const int SpawnerPlayer = 1000;
        [TagField(categoryName = "Spawners/Entities")] public const int SpawnerAlly = 1001;
        [TagField(categoryName = "Spawners/Entities")] public const int SpawnerEnemy = 1002;
        [TagField(categoryName = "Spawners/Entities")] public const int SpawnerBackground = 1003;
        [TagField(categoryName = "Spawners/UI")] public const int SpawnerPlayerBar = 1004;
        [TagField(categoryName = "Spawners/UI")] public const int SpawnerEnemyBar = 1005;
        [TagField(categoryName = "Spawners/UI")] public const int SpawnerAllyBar = 1006;
        [TagField(categoryName = "Spawners/UI")] public const int SpawnerPanel = 1007;
        [TagField(categoryName = "Spawners/Entities")] public const int SpawnerPlayerExploration = 1008;
    }
}
