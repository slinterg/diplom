﻿using Homebrew;

namespace HSG
{
    public static partial class Tag
    {
        [TagField(categoryName = "Entities")]
        public const int Player = 2000;
        [TagField(categoryName = "Entities")]
        public const int Enemy = 2001;
        [TagField(categoryName = "Entities")]
        public const int Ally = 2002;
        [TagField(categoryName = "Entities")]
        public const int Entity = 2003;
    }
}