﻿using System;
using Homebrew;
using UnityEngine;

namespace HSG
{
    public static partial class Tag
    {
        [TagField(categoryName = "States/Entities")]
        public const int Idle = 5000;
        [TagField(categoryName = "States/Entities")]
        public const int Dead = 5001;
        [TagField(categoryName = "States/Entities")]
        public const int Fatality = 5002;

        [TagField(categoryName = "States/Entities")]
        public const int AttackPhysics = 5003;
        [TagField(categoryName = "States/Entities")]
        public const int AttackMagic = 5004;

        [TagField(categoryName = "States/Entities")]
        public const int TakeDamagePhysics = 5005;
        [TagField(categoryName = "States/Entities")]
        public const int TakeDamageMagic = 5006;
        [TagField(categoryName = "States/Entities")]
        public const int DrinkPotion = 5007;
        [TagField(categoryName = "States/Entities")]
        public const int ThrowPotion = 5008;
    }
}
