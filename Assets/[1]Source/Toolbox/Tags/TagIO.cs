﻿using Homebrew;

namespace HSG
{
    public static partial class Tag
    {
        [TagField(categoryName = "IO/Type")]
        public const int IO = 3000;
        [TagField(categoryName = "IO/Type")]
        public const int IO_DisableInBattle = 3001;

        [TagField(categoryName = "IO/State")]
        public const int IO_Disabled = 3002;
    }
}