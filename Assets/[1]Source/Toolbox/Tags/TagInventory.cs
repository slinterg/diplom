﻿using Homebrew;

namespace HSG
{
    public static partial class Tag
    {
        [TagField(categoryName = "Inventory/Action")]
        public const int ItemActionAdd = 10000;
        [TagField(categoryName = "Inventory/Action")]
        public const int ItemActionRemove = 10001;
        [TagField(categoryName = "Inventory/Action")]
        public const int ItemActionMove = 10002;
        [TagField(categoryName = "Inventory/Item/Type")]
        public const int ItemTypeAll = 10004;
        [TagField(categoryName = "Inventory/Action")]
        public const int ClearInventory = 10003;
    }
}
