﻿using UnityEngine;
using Homebrew;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Text;

namespace HSG
{
    public class ProcessingTurn : ProcessingBase, IMustBeWipedOut//, IReceive<SignalClick>, IReceive<SignalEndTurn>
    {
        [GroupBy(Tag.Entity)]
        public Group entity;
        public Queue<Actor> turnQueue = new Queue<Actor>();

        private List<Actor> preQueue;

        private bool isTurned;
        private int round;

        public static Actor nowTurn;

        public ProcessingTurn()
        {

        }


        public void Setup()
        {

        }
    }
}