﻿using Homebrew;
using Sirenix.OdinInspector;

public class ActorExample : Actor
{
	[FoldoutGroup("Setup")] public DataExample dataExample;

	protected override void SetupData()
	{
		Add(dataExample);
	}

	protected override void SetupBehaviors()
	{
		Add<BehaviorExample>();
	}
}