﻿using Homebrew;
using System.Collections.Generic;
using Sirenix.OdinInspector;


namespace HSG
{
    public class ActorTag : Actor
    {
        [FoldoutGroup("Setup")] public List<DataTag> ids;

        protected override void SetupData()
        {
            for (int i = 0; i < ids.Count; i++)
            {
                tags.Add(ids[i].id);
            }
        }

        protected override void SetupBehaviors()
        {

        }
    }
}