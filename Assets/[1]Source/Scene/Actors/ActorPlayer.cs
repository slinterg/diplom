﻿using Homebrew;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using System;

namespace HSG
{
    public class ActorPlayer : Actor
    {
        [FoldoutGroup("Setup")] public List<DataTag> ids;
        [FoldoutGroup("Setup")] public DataMove dataMove;
        [FoldoutGroup("Setup")] public DataStates dataStates;
        [FoldoutGroup("Setup")] public DataStats dataStats;


        protected override void SetupData()
        {            
            //Tags
            for (int i = 0; i < ids.Count; i++)
            {
                tags.Add(ids[i].id);
            }
            //Data
            Add(dataMove);
            Add(dataStates);
            Add(dataStats);

        }

        protected override void SetupBehaviors()
        {
            //Behavior
            Add<BehaviorMove>();
            Add<BehaviorView>();
            //Add<BehaviorInput>();

            //initialized = true;

            Debug.Log("[" + DateTime.Now + " Tick: " + DateTime.Now.Ticks + "]: " + "[" + "COMPONENT" + "] " + "[" + this + "]" + "----Setup End");

        }
    }
}